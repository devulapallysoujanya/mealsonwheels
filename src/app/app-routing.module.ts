import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { NotfoundComponent } from './demo/components/notfound/notfound.component';
import { AppLayoutComponent } from './layout/app.layout.component';
import { LoginPageComponent } from './demo/components/uikit/login/login-page/login-page.component';
import { RegistrationComponent } from './demo/components/uikit/login/registration/registration.component';
@NgModule({
    imports: [
        RouterModule.forRoot(
            [
                {
                    path: '',
                    component: LoginPageComponent,

                    children: [
                       
                    ],
                },
                {
                    path:'registration',
                    component:RegistrationComponent
                },
                {
                    path: 'layout',
                    component: AppLayoutComponent,
                    children: [
                        {
                            path: 'uikit',
                            loadChildren: () =>
                                import(
                                    './demo/components/uikit/uikit.module'
                                ).then((m) => m.UIkitModule),
                        },
                    ],

                },
                // { path: 'notfound', component: NotfoundComponent },
                // { path: '**', redirectTo: '/notfound' },
                // {
                //     path: 'landing',
                //     loadChildren: () =>
                //         import('./demo/components/landing/landing.module').then(
                //             (m) => m.LandingModule
                //         ),
                // },
                
            ],
            
            {
                scrollPositionRestoration: 'enabled',
                anchorScrolling: 'enabled',
                onSameUrlNavigation: 'reload',
            }
        ),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
