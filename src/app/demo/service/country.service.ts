import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CountryService {
    url_ ='assets/demo/data/countries.json'

    constructor(private http: HttpClient) { }

    getCountries() {

        // return this.http.get<any>(this.url_);
        
        return this.http.get<any>('assets/demo/data/countries.json')
            .toPromise()
            .then(res => res as any[])
            .then(data => data);
    }
}
