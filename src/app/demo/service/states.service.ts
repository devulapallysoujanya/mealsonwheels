import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class StateService {
    url_ ='assets/demo/data/states.json'

    constructor(private http: HttpClient) { }

    getStates() {

        // return this.http.get<any>(this.url_);
        
        return this.http.get<any>('assets/demo/data/states.json')
            .toPromise()
            .then(res => res as any[])
            .then(data => data);
    }
}
