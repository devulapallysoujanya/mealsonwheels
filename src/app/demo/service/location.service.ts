import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class LocationService {
    url_ ='assets/demo/data/cities.json'

    constructor(private http: HttpClient) { }

    getLocation() {

        // return this.http.get<any>(this.url_);
        
        return this.http.get<any>('assets/demo/data/cities.json')
            .toPromise()
            .then(res => res as any[])
            .then(data => data);
    }
}
