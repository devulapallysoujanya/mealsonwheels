import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { MenuModule } from 'primeng/menu';

import { DashboardsRoutingModule } from './dashboard-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MenuModule,
        FormsModule,

        DashboardsRoutingModule,
    ],
    declarations: [DashboardComponent],
})
export class DashboardModule {}
