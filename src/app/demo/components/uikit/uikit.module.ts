import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UIkitRoutingModule } from './uikit-routing.module';
import { ButtonModule } from 'primeng/button';
import { MessagesModule } from 'primeng/messages';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';




@NgModule({
	declarations:[
		
	],
	imports: [
		CommonModule,
		UIkitRoutingModule,
		ButtonModule,
		TableModule,
		MessagesModule,
		ToastModule,
		FormsModule,
		ReactiveFormsModule,
		DialogModule,
		
		
		
	]
})
export class UIkitModule {
	ngOnInit(){
		console.log('uikit loaded')
	}
 }
