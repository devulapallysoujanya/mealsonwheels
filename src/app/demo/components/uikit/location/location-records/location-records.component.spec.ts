import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationRecordsComponent } from './location-records.component';

describe('LocationRecordsComponent', () => {
  let component: LocationRecordsComponent;
  let fixture: ComponentFixture<LocationRecordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationRecordsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LocationRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
