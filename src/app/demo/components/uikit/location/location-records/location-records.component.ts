import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Table } from 'primeng/table';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-location-records',
  templateUrl: './location-records.component.html',
  styleUrls: ['./location-records.component.scss']
})
export class LocationRecordsComponent {
  routeItems: MenuItem[] = [];
  cols!: any[];
  activeItem!: MenuItem;
  locationList: Location[] = [];


  constructor(private api:ApiService){}

  ngOnInit(){
    this.getLocationList()
    this.routeItems = [
      { label: 'location', routerLink: ['/layout/uikit/location',],routerLinkActiveOptions:{exact:true} },
      { label: 'Records', routerLink: '/layout/uikit/location/locationRecords',routerLinkActiveOptions:{exact:true} },
  ];
  
  
  
  this.cols = [
    { field: 'Club id', header: 'Club Id' },

    { field: 'locationId', header: 'Location Id' },
    { field: 'locationName', header: 'Location Name' },
    { field: 'locationChairman', header: 'Location chairPerson' },
    { field: 'locationChairman', header: 'Location Address' },
];
  }

  getLocationList(){
    this.api.getLocationData().subscribe((res) => {
      this.locationList = res;
      console.log(this.locationList);
  });
  }

  clear(table: Table) {
    table.clear();
}
}
