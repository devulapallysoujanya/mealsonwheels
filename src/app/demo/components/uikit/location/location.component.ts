import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { error } from 'console';
import { MenuItem, MessageService } from 'primeng/api';
import { Club } from 'src/app/models/clubs.model';
import { Location } from 'src/app/models/location.model';
import { ApiService } from 'src/app/service/api.service';


@Component({
    templateUrl: './location.component.html'
})
export class LocationComponent implements OnInit {
    
    clubsList: Club[] = [];
    locationData: any[] = [];

    location = new Location('','','','')
    routeItems: MenuItem[] = [];
    filteredClubs: any[] = [];
    

    constructor(private messageService:MessageService,private api:ApiService) {
        
       
    }

    ngOnInit() {
        this.getClubsData()
        this.routeItems = [
            { label: 'Location', routerLink: ['/layout/uikit/location'] },
            { label: 'Records', routerLink: '/layout/uikit/location/locationRecords' },
        ];
    }
    filterClubs(event: any) {
        const filtered: any[] = [];
        const query = event.query;
        for (let i = 0; i < this.clubsList.length; i++) {
            const club = this.clubsList[i];
            // console.log(club)
            // console.log(region.id)
            if (club.clubName.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(club);
                //  console.log(filtered);
            }
        }

        this.filteredClubs = filtered;
        // console.log(this.filteredClubs)
    }

    getClubsData() {
        this.api.getClubData().subscribe((res) => {
            this.clubsList = res;
        console.log(this.clubsList);
        });
    }
    postLocationData(data: Location) {
        console.log(data);
        this.location.clubId = data.clubId.clubId
        this.api.postLocationData(this.location).subscribe((res) => {
            // console.log(res);
            this.showSuccess();
        },error=>{
            this.showError()
        });
    }
    showSuccess() {
        this.messageService.add({
            severity: 'success',
            summary: 'success',
            detail: 'Location has beeen added successfully',
        });
    }
    showError() {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Message Content' });
    }
    
    
}
