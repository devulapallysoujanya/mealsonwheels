import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LocationComponent } from './location.component';
import { LocationRecordsComponent } from './location-records/location-records.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: LocationComponent },
            { path: 'locationRecords', component: LocationRecordsComponent },
        ]),
    ],
    exports: [RouterModule],
})
export class LocationRoutingModule {}
