import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BookingsComponent } from './bookings.component';
import { BookingRecordsComponent } from './booking-records/booking-records.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: BookingsComponent },
            { path: 'bookingRecords', component: BookingRecordsComponent },
        ]),
    ],
    exports: [RouterModule],
})
export class BookingsRoutingModule {}
