import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BookingsComponent } from './bookings.component';
import { BookingsRoutingModule } from './bookings-routing.module';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { TableModule } from 'primeng/table';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { SidebarModule } from 'primeng/sidebar';
import { RippleModule } from 'primeng/ripple';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextModule } from 'primeng/inputtext';
import { ScheduleModule } from '@syncfusion/ej2-angular-schedule';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { DividerModule } from 'primeng/divider';
import { BookingRecordsComponent } from './booking-records/booking-records.component';
import { TabMenuModule } from 'primeng/tabmenu';


@NgModule({
    imports: [
        CommonModule,
        BookingsRoutingModule,
        ToastModule,
        DialogModule,
        FormsModule,
        TooltipModule,
        InputTextModule,
        ButtonModule,
        OverlayPanelModule,
        TableModule,
        ConfirmDialogModule,
        SidebarModule,
        RippleModule,
        ConfirmPopupModule,
        ScheduleModule,
        CalendarModule,
        DropdownModule,
        AutoCompleteModule,
        DividerModule,
        TabMenuModule
    ],
    declarations: [BookingsComponent, BookingRecordsComponent],
})
export class BookingsModule {}
