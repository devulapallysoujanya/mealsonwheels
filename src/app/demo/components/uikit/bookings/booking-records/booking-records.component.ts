import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Table } from 'primeng/table';
import { Bookings } from 'src/app/models/bookings.model';
import { ApiService } from 'src/app/service/api.service';

@Component({
    selector: 'app-booking-records',
    templateUrl: './booking-records.component.html',
    styleUrls: ['./booking-records.component.scss'],
})
export class BookingRecordsComponent {
    routeItems: MenuItem[] = [];
    bookingsList: Bookings[] = [];

    constructor(private api: ApiService) {}

    ngOnInit() {
        this.getBookingsList();
        this.routeItems = [
            {
                label: 'Bookings',
                routerLink: ['/layout/uikit/booking'],
                routerLinkActiveOptions: { exact: true },
            },
            {
                label: 'Records',
                routerLink: '/layout/uikit/booking/bookingRecords',
                routerLinkActiveOptions: { exact: true },
            },
        ];
    }

    getBookingsList() {
        this.api.getRecordsData().subscribe((res) => {
            this.bookingsList = res;
            console.log(this.bookingsList);
        });
    }

    clear(table: Table) {
        table.clear();
    }

    onEdit(event:any){
        console.log(event)

    }
}
