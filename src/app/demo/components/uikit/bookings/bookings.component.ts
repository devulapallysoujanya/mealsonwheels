import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { Club } from 'src/app/models/clubs.model';
import { Zones } from 'src/app/models/zones.model';
import { ApiService } from 'src/app/service/api.service';
import { Location } from 'src/app/models/location.model';
import { Bookings } from 'src/app/models/bookings.model';
import { NgForm } from '@angular/forms';
import { error } from 'console';

@Component({
    templateUrl: './bookings.component.html',
    providers: [ConfirmationService, MessageService],
})
export class BookingsComponent implements OnInit {
    time!: any[];
    regionsData: any[] = [];
    filteredRegions: any[] = [];
    zone = new Zones(0, '', '');
    zonesList: Zones[] = [];
    club = new Club('', '', '');
    location = new Location('', '', '', '');
    bookings = new Bookings(0,0,0,0,new Date(),'','','','',0,false);

    filteredZones: any[] = [];
    filteredClubs: any[] = [];
    filteredLocations: any[] = [];

    selectedRegion: any;
    selectedZone: any;
    selectedClub: any;

    clubsList: Club[] = [];
    locationsList: Location[] = [];
    routeItems: MenuItem[] = [];
    minimumDate = new Date();


    constructor(
        private api: ApiService,
        private messageService: MessageService
    ) {}

    ngOnInit() {
        this.getRegionsData();
       // this.getZonesData();
        this.getClubsData();
        this.getLocationList();

        this.time = [
            { name: 'BreakFast' },
            { name: 'Lunch' },
            { name: 'Dinner' },
        ];
        this.routeItems = [
            {
                label: 'Bookings',
                routerLink: ['/layout/uikit/booking'],
                routerLinkActiveOptions: { exact: true },
            },
            {
                label: 'Records',
                routerLink: '/layout/uikit/booking/bookingRecords',
                routerLinkActiveOptions: { exact: true },
            },
        ];
        //console.log(this.time);
    }

    // code for regions dropdown

    getRegionsData() {
        this.api.getRegionsData().subscribe((res) => {
            this.regionsData = res;
            //console.log(this.regionsData);
        });
    }

    filterRegion(event: any) {
        const filtered: any[] = [];
        const query = event.query;
        for (let i = 0; i < this.regionsData.length; i++) {
            const region = this.regionsData[i];

            if (
                region.regionName.toLowerCase().indexOf(query.toLowerCase()) ==
                0
            ) {
                filtered.push(region);
                // console.log(filtered);
            }
        }

        this.filteredRegions = filtered;
    }
    onSelectRegion(event: any) {
        //console.log(event);
        this.selectedRegion = event.regionId;
        console.log(this.selectedRegion);
        //this.bookings.regionDetail = event.id;
        // console.log(this.bookings.regionDetail);
        this.getFilteredZonesData();
    }
    // code for zones dropdown

    getZonesData() {
        this.api.getZoneData().subscribe((res) => {
            this.zonesList = res;

            //console.log(this.zonesList);
        });
    }

    filterZone(event: any) {
        const filtered: any[] = [];
        const query = event.query;
        for (let i = 0; i < this.zonesList.length; i++) {
            const zone = this.zonesList[i];
            // console.log(region.id)
            if (zone.zoneName.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(zone);
                // console.log(filtered);
            }
        }

        this.filteredZones = filtered;
        if(this.filteredZones.length == 0){
            alert('please select a region')
        }
    }

    getFilteredZonesData() {
        this.api.getZoneData().subscribe((res) => {
            console.log(res);
            this.zonesList = res.filter((e: { regionId: any }) => {
                return e.regionId == this.selectedRegion;
            });
            // this.zonesList = res.filter(
            //     (e: { regionDetail: { id: any } }) =>
            //         e.regionDetail.id == this.selectedRegion
            // );

            console.log(this.zonesList);
        });
    }
    onSelectZone(event: any) {
        //console.log(event);
        this.selectedZone = event.zoneId;
        //console.log(this.selectedZone)
        //this.bookings.zoneDetail = this.selectedZone;
        this.getFilteredClubsData();
    }

    // code for clubs dropdown

    getClubsData() {
        this.api.getClubData().subscribe((res) => {
            this.clubsList = res;
            //console.log(this.clubsList);
        });
    }

    getFilteredClubsData() {
        this.api.getClubData().subscribe((res) => {
            this.clubsList = res.filter((e: { zoneId: any }) => {
                return e.zoneId == this.selectedZone;
            });
            console.log(this.clubsList);
        });
    }

    filterClubs(event: any) {
        const filtered: any[] = [];
        const query = event.query;
        for (let i = 0; i < this.clubsList.length; i++) {
            const club = this.clubsList[i];
            // console.log(club)
            // console.log(region.id)
            if (club.clubName.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(club);
                //  console.log(filtered);
            }
        }

        this.filteredClubs = filtered;
        // console.log(this.filteredClubs)
    }
    onSelectClub(event: any) {
        //console.log(event);
        this.selectedClub = event.clubId;
        //console.log(this.selectedClub)
        this.getFilteredLocationList();
    }

    // code for locationdropdown

    filterLocations(event: any) {
        const filtered: any[] = [];
        const query = event.query;
        for (let i = 0; i < this.locationsList.length; i++) {
            const location = this.locationsList[i];
            // console.log(club)
            // console.log(region.id)
            if (
                location.locationName
                    .toLowerCase()
                    .indexOf(query.toLowerCase()) == 0
            ) {
                filtered.push(location);
                // console.log(filtered);
            }
        }

        this.filteredLocations = filtered;
        // console.log(this.filteredLocations)
    }

    getLocationList() {
        this.api.getLocationData().subscribe((res) => {
            this.locationsList = res;
            //console.log(this.locationsList);
        });
    }

    getFilteredLocationList() {
        this.api.getLocationData().subscribe((res) => {
            this.locationsList = res.filter((e: { clubId: any }) => {
                return e.clubId == this.selectedClub;
            });
            //console.log(this.locationsList);
        });
    }

    onSubmitbooking(data: NgForm) {
        //console.log(data);
    }
    postBookingsData(data: Bookings) {
        //console.log(data);
        this.bookings.regionId = data.regionId.regionId;
        this.bookings.zoneId = data.zoneId.zoneId;
        this.bookings.clubId = data.clubId.clubId;
        this.bookings.locationId = data.locationId.locationId;
        this.bookings.time = data.time.name;
        console.log(typeof this.bookings.amount);
        this.api.postBookingsData(this.bookings).subscribe((res) => {
            console.log(res);
            this.showSuccess();
        },error=>{
            this.showError()
        });
    }
    showSuccess() {
        this.messageService.add({
            severity: 'success',
            summary: 'success',
            detail: 'Booking has beeen added successfully',
        });
    }

    showError() {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Something Went Wrong ' });
    }
}
