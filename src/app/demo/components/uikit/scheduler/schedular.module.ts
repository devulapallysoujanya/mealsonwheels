import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SchedularComponent } from './schedular.component';
import { ScheduleRoutingModule } from './schedular-routing.module';

import { ScheduleModule } from '@syncfusion/ej2-angular-schedule';

@NgModule({
    imports: [
        CommonModule,
        ScheduleRoutingModule,
        FormsModule,

        ScheduleModule,
    ],
    declarations: [SchedularComponent],
})
export class ScedularModule {}
