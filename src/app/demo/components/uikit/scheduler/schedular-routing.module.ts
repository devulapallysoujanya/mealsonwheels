import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SchedularComponent } from './schedular.component';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: SchedularComponent }]),
    ],
    exports: [RouterModule],
})
export class ScheduleRoutingModule {}
