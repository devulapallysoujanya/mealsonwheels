import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { User } from 'src/app/models/user.model';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent {
  user=new User('','','','',false,'')

  constructor(private route:Router, private api:ApiService,private messageService:MessageService){}
  
  
  
  
  
  onSubmit(user:User){
//    this.showSuccess();

    console.log(user)
    this.api.postRegistrationData(user).subscribe((res) => {
      console.log(res)
   this.route.navigateByUrl('/layout/uikit/regions')
  });
    

  }
  showSuccess() {
    this.messageService.add({
        severity: 'success',
        summary: 'success',
        detail: 'Login Successfully',
    });
}


  

}
