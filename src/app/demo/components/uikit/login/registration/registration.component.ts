import { Component } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Club } from 'src/app/models/clubs.model';
import { User } from 'src/app/models/user.model';
import { ApiService } from 'src/app/service/api.service';
interface isClubMember {
    name: string;
    value: boolean;
}

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent {

    clubsList: Club[] = [];

    
    formSubmitted = false;
    registrationFormGroup = new FormGroup({
        userName: new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(50),
        ]),
        email: new FormControl('', [Validators.required, Validators.email]),
        clubId: new FormControl('', [Validators.required]),
        address: new FormControl('', [Validators.required]),
        password: new FormControl('', [
            Validators.required,
            // Validators.pattern(
            //     '((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15})'
            // ),
        ]),
    });


    constructor(
        private router: Router,
        private api: ApiService,
        private messageService: MessageService
    ) {}

    ngOnInit() {
        this.getClubsData();
        
    }
    onSubmit(): void {
        this.formSubmitted = true;
        if (this.registrationFormGroup.invalid) {
            return;
        }

        let user = this.registrationFormGroup.value;
        //console.log(this.registrationFormGroup.value)
        this.api.postRegistrationData(user).subscribe((res) => {
            console.log(res);
            this.showSuccess();

            setTimeout(()=>{
                this.router.navigateByUrl('')
            },500)



        });
    }

    showSuccess() {
        this.messageService.add({
            severity: 'success',
            summary: 'success',
            detail: 'Registered successfully',
        });
    }
    getClubsData() {
        this.api.getClubData().subscribe((res) => {
            this.clubsList = res;
        console.log(this.clubsList);
        });
    }
}
