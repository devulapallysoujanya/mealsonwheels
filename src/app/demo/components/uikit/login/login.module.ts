import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginPageComponent } from './login-page/login-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PasswordModule } from 'primeng/password';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { RegistrationComponent } from './registration/registration.component';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';



@NgModule({
  declarations: [
    LoginPageComponent,
    RegistrationComponent,
    
  
  ],
  imports: [
    CommonModule,
    FormsModule,
    LoginRoutingModule ,
    PasswordModule,
    ReactiveFormsModule,
    CardModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    ToastModule

  ]
})
export class LoginModule { }
