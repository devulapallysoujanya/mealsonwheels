import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RegionsComponent } from './regions.component';
import { RegionRecordsComponent } from './region-records/region-records.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: RegionsComponent },
            { path: 'regionRecords', component: RegionRecordsComponent }
        ]),
    ],
    exports: [RouterModule],
})
export class RegionsRoutingModule {}
