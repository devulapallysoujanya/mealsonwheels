import { Component } from '@angular/core';
import { error } from 'console';
import { MessageService, SortEvent } from 'primeng/api';
import { MenuItem } from 'primeng/api/menuitem';
import { Table } from 'primeng/table';
import { ApiService } from 'src/app/service/api.service';

@Component({
    selector: 'app-region-records',
    templateUrl: './region-records.component.html',
    styleUrls: ['./region-records.component.scss'],
})
export class RegionRecordsComponent {
    regionsData: any[] = [];
    routeItems: MenuItem[] = [];
    activeItem!: MenuItem;
    cols!: any[];
    loading: boolean = true;

    activityValues: number[] = [0, 100];
    dt1!: Table;


    constructor(
        private api: ApiService,
        private messageService:MessageService
    ) {}

    ngOnInit() {
        this.getRegionsData();
        this.routeItems = [
            { label: 'Regions', routerLink: ['/layout/uikit/regions'] ,routerLinkActiveOptions:{exact:true}},
            { label: 'Records', routerLink: '/layout/uikit/regions/regionRecords',routerLinkActiveOptions:{exact:true} },
        ];
        this.cols = [
            { field: 'regionId', header: 'regionId' },

            // { field: 'regionId', header: 'RegionId' },
            { field: 'regionName', header: 'Region Name' },
            { field: 'regionChairpersonDetails', header: 'Region chairPerson' },
        ];
        //     this.productService.getProductsSmall().then((data) => {
        //         this.products = data;
        //         console.log(this.products)

        // })
    }

    getRegionsData() {
        this.api.getRegionsData().subscribe((res) => {
            this.regionsData = res;
            console.log(this.regionsData);
        },error=>{
            this.showError()
        });
    }
    clear(table: Table) {
        table.clear();
    }

    showError() {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Something went wrong' });
    }

    

}
