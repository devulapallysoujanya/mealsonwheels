import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionRecordsComponent } from './region-records.component';

describe('RegionRecordsComponent', () => {
  let component: RegionRecordsComponent;
  let fixture: ComponentFixture<RegionRecordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegionRecordsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegionRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
