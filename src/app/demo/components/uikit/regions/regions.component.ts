import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { error } from 'console';
import { MenuItem, MessageService } from 'primeng/api';
import { Region } from 'src/app/models/region.model';
import { ApiService } from 'src/app/service/api.service';

@Component({
    templateUrl: './regions.component.html',
    styleUrls: ['./regions.component.html'],
})
export class RegionsComponent implements OnInit {
    items: MenuItem[] = [];
    routeItems: MenuItem[] = [];
    region = new Region('', '','');

    activeItem!: MenuItem;
    constructor(
        private api: ApiService,
        private messageService: MessageService
    ) {}

    ngOnInit() {
//console.log(this.activeItem)
        this.routeItems = [
            { label: 'Regions', routerLink: ['/layout/uikit/regions'],routerLinkActiveOptions:{exact:true}},
            { label: 'Records', routerLink: '/layout/uikit/regions/regionRecords',routerLinkActiveOptions:{exact:true} },
        ];


    }
    postRegionData(data: NgForm) {

        this.api.postRegionData(data).subscribe((res) => {
            console.log(res.id)
         this.showSuccess();
        },error=>{
            this.showError()
        });
    }
    showSuccess() {
        this.messageService.add({
            severity: 'success',
            summary: 'success',
            detail: 'Region has beeen added successfully',
        });
    }

    showError() {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Something went wrong' });
    }

}
