import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'club',
                data: { breadcrumb: 'club' },
                loadChildren: () =>
                    import('./club/club.module').then(
                        (m) => m.ClubsModule
                    ),
            },
            {
                path: 'regions',
                data: { breadcrumb: 'regions' },
                loadChildren: () =>
                    import('./regions/regions.module').then(
                        (m) => m.RegionsModule
                    ),
            },
            {
                path: 'zones',
                data: { breadcrumb: 'zones' },
                loadChildren: () =>
                    import('./zones/zones.module').then((m) => m.ZonesModule),
            },
            {
                path: 'location',
                data: { breadcrumb: 'location' },
                loadChildren: () =>
                    import('./location/location.module').then(
                        (m) => m.LocationModule
                    ),
            },
            
            {
                path: 'schedular',
                data: { breadcrumb: 'schedular' },
                loadChildren: () =>
                    import('./scheduler/schedular.module').then(
                        (m) => m.ScedularModule
                    ),
            },
            {
                path: 'booking',
                data: { breadcrumb: 'bookings' },
                loadChildren: () =>
                    import('./bookings/bookings.module').then(
                        (m) => m.BookingsModule
                    ),
            },
            {
                path: 'login',
                data: { breadcrumb: 'login' },
                loadChildren: () =>
                    import('./login/login.module').then(
                        (m) => m.LoginModule
                    ),
            },
            

            //{ path: '**', redirectTo: '/notfound' },
        ]),
    ],
    exports: [RouterModule],
})
export class UIkitRoutingModule {}
