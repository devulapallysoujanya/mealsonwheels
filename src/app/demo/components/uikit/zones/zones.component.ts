import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { error } from 'console';
import { MenuItem, MessageService, SelectItem } from 'primeng/api';
import { Region } from 'src/app/models/region.model';
import { Zones } from 'src/app/models/zones.model';
import { ApiService } from 'src/app/service/api.service';

@Component({
    templateUrl: './zones.component.html',
})
export class ZonesComponent implements OnInit {
    regionsList: Region[] = [];
    zone = new Zones(0,  '', '');

    filteredRegions: any[] = [];

    routeItems: MenuItem[] = [];

    constructor(
        private api: ApiService,
        private messageService: MessageService
    ) {}

    ngOnInit() {
        this.getRegionsData();
        this.routeItems = [
            {
                label: 'Zones',
                routerLink: ['/layout/uikit/zones'],
                routerLinkActiveOptions: { exact: true },
            },
            {
                label: 'Records',
                routerLink: '/layout/uikit/zones/zoneRecords',
                routerLinkActiveOptions: { exact: true },
            },
        ];
    }

    filterRegion(event: any) {
        const filtered: any[] = [];
        const query = event.query;
        for (let i = 0; i < this.regionsList.length; i++) {
            const region = this.regionsList[i];

            if (
                region.regionName.toLowerCase().indexOf(query.toLowerCase()) ==
                0
            ) {
                filtered.push(region);
                // console.log(filtered);
            }
        }

        this.filteredRegions = filtered;
    }

    getRegionsData() {
        this.api.getRegionsData().subscribe((res) => {
            this.regionsList = res;
            console.log(this.regionsList);
        },error=>{
            this.showError()
        });
    }
    postZonesData(data: Zones) {
    console.log(data.regionId.regionId);
this.zone.regionId = data.regionId.regionId
        //console.log(this.zone)
        this.api.postZoneData(this.zone).subscribe((res) => {
            // console.log(res);
            this.showSuccess();
        },error=>{
            this.showError()
        });
    }
    showSuccess() {
        this.messageService.add({
            severity: 'success',
            summary: 'success',
            detail: 'Zone has beeen added successfully',
        });
    }
    showError() {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Message Content' });
    }

}
