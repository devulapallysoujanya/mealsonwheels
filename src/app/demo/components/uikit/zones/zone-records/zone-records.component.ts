import { Component } from '@angular/core';
import { error } from 'console';
import { MenuItem, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { Region } from 'src/app/models/region.model';
import { Zones } from 'src/app/models/zones.model';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-zone-records',
  templateUrl: './zone-records.component.html',
  styleUrls: ['./zone-records.component.scss']
})
export class ZoneRecordsComponent {
  routeItems: MenuItem[] = [];
  zoneList: Zones[] = [];
  cols!: any[];
  activeTab!: MenuItem;
  public index!: number;
  visible!: boolean;
  selectedRegion= new Region('','','')

  




constructor(private api:ApiService,private messageService:MessageService){}
  ngOnInit(){
    
    this.getZoneData()
    
  this.routeItems = [
      { label: 'Zone', routerLink: ['/layout/uikit/zones'],routerLinkActiveOptions:{exact:true} },
      { label: 'Records', routerLink: '/layout/uikit/zones/zoneRecords',routerLinkActiveOptions:{exact:true} },
  ];
  this.cols = [
     { field:'regionId.regionId', header: 'Region Id' },

     { field: 'zoneId', header: 'Zone Id' },
    { field: 'zoneName', header: 'Zone Name' },
    { field: 'zoneChairpersonDetails', header: 'Zone chairPerson' },
];
  }
  getZoneData(){
    this.api.getZoneData().subscribe((res) => {
      this.zoneList = res;
      // console.log(this.zoneList);
  },error=>{
    this.showError()
  });


  }

  showDialog(id:any) {
    this.api.getRegionsDataById(id).subscribe((res)=>{
      //console.log(res);
      this.selectedRegion = res
      console.log(this.selectedRegion)
    })
      this.visible = true;
  }
 
  clear(table: Table) {
    table.clear();
}
showError() {
  this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Message Content' });
}

  
}
