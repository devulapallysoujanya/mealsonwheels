import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneRecordsComponent } from './zone-records.component';

describe('ZoneRecordsComponent', () => {
  let component: ZoneRecordsComponent;
  let fixture: ComponentFixture<ZoneRecordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZoneRecordsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ZoneRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
