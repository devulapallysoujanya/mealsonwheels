import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ZonesComponent } from './zones.component';
import { ZoneRecordsComponent } from './zone-records/zone-records.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: ZonesComponent },
            { path: 'zoneRecords', component: ZoneRecordsComponent },
        ]),
    ],
    exports: [RouterModule],
})
export class ZonesRoutingModule {}
