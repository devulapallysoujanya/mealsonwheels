import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClubRecordsComponent } from './club-records.component';

describe('ClubRecordsComponent', () => {
  let component: ClubRecordsComponent;
  let fixture: ComponentFixture<ClubRecordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClubRecordsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClubRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
