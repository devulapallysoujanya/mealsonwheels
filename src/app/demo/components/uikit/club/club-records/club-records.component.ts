import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Table } from 'primeng/table';
import { Club } from 'src/app/models/clubs.model';
import { ApiService } from 'src/app/service/api.service';

@Component({
    selector: 'app-club-records',
    templateUrl: './club-records.component.html',
    styleUrls: ['./club-records.component.scss'],
})
export class ClubRecordsComponent {
    routeItems: MenuItem[] = [];
    clubsData: any[] = [];
    clubsList: Club[] = [];
    cols!: any[];
    activeItem!: MenuItem;

    constructor(private api: ApiService) {}

    ngOnInit() {
        this.getClubsData();
        this.routeItems = [
            {
                label: 'clubs',
                routerLink: ['/layout/uikit/club'],
                routerLinkActiveOptions: { exact: true },
            },
            {
                label: 'Records',
                routerLink: '/layout/uikit/club/clubRecords',
                routerLinkActiveOptions: { exact: true },
            },
        ];

        this.cols = [
            { field: 'Zone Id', header: 'Zone Id' },

            { field: 'clubId', header: 'Club Id' },
            { field: 'clubName', header: 'Club Name' },
            { field: 'clubChairPerson', header: 'Club chairPerson' },
        ];
    }

    getClubsData() {
        this.api.getClubData().subscribe((res) => {
            this.clubsList = res;
            console.log(this.clubsList);
        });
    }

    clear(table: Table) {
        table.clear();
    }
}
