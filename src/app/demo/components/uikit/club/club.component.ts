import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { error } from 'console';
import { MenuItem, MessageService } from 'primeng/api';

import { Club } from 'src/app/models/clubs.model';
import { Zones } from 'src/app/models/zones.model';
import { ApiService } from 'src/app/service/api.service';

@Component({
    templateUrl: './club.component.html',
    styleUrls:['./club.component.css']
})
export class ClubComponent implements OnInit {
    zonesList: Zones[] = [];
    clubsData: any[] = [];

    club = new Club('', '', '');

   



    filteredZones: any[] = [];
    
    routeItems: MenuItem[] = [];

    constructor(private api: ApiService,private messageService:MessageService) {}

    ngOnInit() {
        this.getZonesData();
        this.routeItems = [
            { label: 'Clubs', routerLink: ['/layout/uikit/club',],routerLinkActiveOptions:{exact:true} },
            { label: 'Records', routerLink: '/layout/uikit/club/clubRecords',routerLinkActiveOptions:{exact:true} },
        ];
    }

    filterZone(event: any) {
        const filtered: any[] = [];
        const query = event.query;
        for (let i = 0; i < this.zonesList.length; i++) {
           
            const zone = this.zonesList[i];
            if(zone.zoneName == null){
                continue
            }
            console.log(zone.zoneName)
            if (zone.zoneName.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(zone);
                // console.log(filtered);
            }
        }

        this.filteredZones = filtered;
    }
    postClubsData(data: Club) {
        // console.log(data);
        this.club.zoneId = data.zoneId.zoneId
        console.log(this.club)
        this.api.postClubData(this.club).subscribe((res) => {
            // console.log(res);
            this.showSuccess();
        },error=>{
            this.showError()
        });
    }

    getZonesData() {
        this.api.getZoneData().subscribe((res) => {
            this.zonesList = res;
             console.log(this.zonesList);
        });
    }

    showSuccess() {
        this.messageService.add({
            severity: 'success',
            summary: 'success',
            detail: 'Club has beeen added successfully',
        });
    }
    showError() {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Message Content' });
    }
}
