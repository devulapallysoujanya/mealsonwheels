import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ClubComponent } from './club.component';
import { ClubRecordsComponent } from './club-records/club-records.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: ClubComponent },
            { path: 'clubRecords', component: ClubRecordsComponent },
        ]),
    ],
    exports: [RouterModule],
})
export class FloatlabelDemoRoutingModule {}
