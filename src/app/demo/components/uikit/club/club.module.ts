import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ClubComponent } from './club.component';
import { FloatlabelDemoRoutingModule } from './club-routing.module';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CalendarModule } from 'primeng/calendar';
import { ChipsModule } from 'primeng/chips';
import { DropdownModule } from 'primeng/dropdown';
import { InputMaskModule } from 'primeng/inputmask';
import { InputNumberModule } from 'primeng/inputnumber';
import { CascadeSelectModule } from 'primeng/cascadeselect';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { TabMenuModule } from 'primeng/tabmenu';
import {  ToastModule } from 'primeng/toast';
import { ClubRecordsComponent } from './club-records/club-records.component';
import { TableModule } from 'primeng/table';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        FloatlabelDemoRoutingModule,
        AutoCompleteModule,
        CalendarModule,
        ChipsModule,
        
        InputTextareaModule,
        InputTextModule,
        TabMenuModule,
        ToastModule,
        TableModule
        
    ],
    declarations: [ClubComponent, ClubRecordsComponent],
})
export class ClubsModule {}
