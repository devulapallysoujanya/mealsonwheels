export class Club {
    constructor(
        public zoneId:any,
       
        public clubName: string,
        public clubChairpersonDetails: string
    ) {}
}
