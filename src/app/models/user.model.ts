export class User {
    constructor(
    
        public userName: string,
        public email: string,
        public mobile:string,
        public password:any,
        public member:boolean,
        public address:string
    ) {}
}
