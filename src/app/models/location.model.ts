export class Location {
    constructor(
        public clubId:any,
        
        public  locationName: string,
        public locationInchargeDetails: string,
        public locationAddress:string
    ) {}
}
