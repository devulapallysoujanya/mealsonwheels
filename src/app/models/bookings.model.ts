export class Bookings {
    constructor(
        public regionId:any,
        public zoneId:any,
        public clubId:any,
        public locationId:any,
        
        public  bookingDate:Date,
        public time :any,
        public sponsorName: string,
        public sponsorEmail: string,
        public sponsorPhoneNumber: string,
        public amount: number,
        public status:boolean
    ) {}
}
