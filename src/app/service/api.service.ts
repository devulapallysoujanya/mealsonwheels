import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
// Testing the file

@Injectable({
    providedIn: 'root',
})
export class ApiService {
    constructor(private http: HttpClient) {}

    postRegionData(data: any) {
        return this.http
            .post<any>(
                'http://localhost:8085/mealsonwheels/api/v1/region',
                data
            )
            .pipe(
                map((res: any) => {
                    return res;
                })
            );
    }

    getRegionsData() {
        return this.http
            .get<any>('http://localhost:8085/mealsonwheels/api/v1/region')
            .pipe(
                map((res: any) => {
                    console.log(res);
                    return res;
                })
            );
    }
    getRegionsDataById(id: any) {
        return this.http
            .get<any>('http://localhost:8085/mealsonwheels/api/v1/region/' + id)
            .pipe(
                map((res: any) => {
                    return res;
                })
            );
    }

    // zone

    postZoneData(data: any) {
        return this.http
            .post<any>('http://localhost:8085/mealsonwheels/api/v1/zone', data)
            .pipe(
                map((res: any) => {
                    return res;
                })
            );
    }

    getZoneData() {
        return this.http
            .get<any>('http://localhost:8085/mealsonwheels/api/v1/zone')
            .pipe(
                map((res: any) => {
                    // console.log(res)
                    return res;
                })
            );
    }

    // clubs service

    postClubData(data: any) {
        return this.http
            .post<any>('http://localhost:8085/mealsonwheels/api/v1/club', data)
            .pipe(
                map((res: any) => {
                    return res;
                })
            );
    }

    getClubData() {
        return this.http
            .get<any>('http://localhost:8085/mealsonwheels/api/v1/club')
            .pipe(
                map((res: any) => {
                    // console.log(res)
                    return res;
                })
            );
    }

    // location service
    postLocationData(data: any) {
        return this.http
            .post<any>(
                'http://localhost:8085/mealsonwheels/api/v1/location',
                data
            )
            .pipe(
                map((res: any) => {
                    console.log(res);

                    return res;
                })
            );
    }

    getLocationData() {
        return this.http
            .get<any>('http://localhost:8085/mealsonwheels/api/v1/location')
            .pipe(
                map((res: any) => {
                    console.log(res);
                    return res;
                })
            );
    }

    // bookings
    postBookingsData(data: any) {
        return this.http
            .post<any>(
                'http://localhost:8085/mealsonwheels/api/v1/booking',
                data
            )
            .pipe(
                map((res: any) => {
                    console.log(res);

                    return res;
                })
            );
    }
    getRecordsData() {
        return this.http
            .get<any>('http://localhost:8085/mealsonwheels/api/v1/booking')
            .pipe(
                map((res: any) => {
                    console.log(res);
                    return res;
                })
            );
    }

    //registration

    postRegistrationData(data: any) {
        return this.http
            .post<any>('http://localhost:8085/mealsonwheels/api/v1/user', data)
            .pipe(
                map((res: any) => {
                    return res;
                })
            );
    }

    getUserData() {
        return this.http
            .get<any>('http://localhost:8085/mealsonwheels/api/v1/user')
            .pipe(
                map((res: any) => {
                    console.log(res);
                    return res;
                })
            );
    }
}
