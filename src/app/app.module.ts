import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppLayoutModule } from './layout/app.layout.module';
import { NotfoundComponent } from './demo/components/notfound/notfound.component';
import { CountryService } from './demo/service/country.service';
import { EventService } from './demo/service/event.service';
// import { IconService } from './demo/service/icon.service';
// import { NodeService } from './demo/service/node.service';
import { TableModule } from 'primeng/table';



import { ScheduleModule } from '@syncfusion/ej2-angular-schedule';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StateService } from './demo/service/states.service';
import { LocationService } from './demo/service/location.service';
import { MessagesModule } from 'primeng/messages';
import { ApiService } from './service/api.service';
import { MessageService } from 'primeng/api';


@NgModule({
    declarations: [AppComponent, NotfoundComponent, ],
    imports: [
        AppRoutingModule,
        AppLayoutModule,

        HttpClientModule,
        FormsModule,
        TableModule,
        
        
        
        ReactiveFormsModule,
        MessagesModule,

    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        CountryService,
        
        EventService,
        // IconService,
        // NodeService,
        StateService,
        LocationService,
        ApiService,
        MessageService,
        
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
